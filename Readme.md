[![Build Status](http://bindbuid.cloudapp.net/buildStatus/icon?job=dojo-look-and-say)](http://bindbuid.cloudapp.net/job/dojo-look-and-say/)

   Este projeto � um template do tipo Get Started para a resolu��o do problema [Look and Say](http://en.wikipedia.org/wiki/Look-and-say_sequence).  

A solu��o consiste em dois projetos, o primeiro com a classe base para resoluçao e um projeto de testes com varios níveis sendo o último de responsabilidade do candidato implementar o teste e a resoluçao para o mesmo.

## Links �teis

 * [For code review](http://bind-upsource.cloudapp.net/DLAS/view)
 * [For issues](http://bind-youtrack.cloudapp.net/issues?q=project%3A+dojo-look-and-say)
 * [For build server](http://jenkins.bindsolution.com/view/Dojos/job/dojo-look-and-say/)
 * [For source](http://sources.bindsolution.com/look-and-say/overview)

